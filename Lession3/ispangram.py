import string


def ispangram(str1, alphabet=string.ascii_lowercase):
    alpha = set(alphabet)
    strdata = set(str1.lower())
    return alpha <= strdata


print(ispangram("The quick brown fox jumps over the lazy dog"))
